#!/bin/bash

# Actualizar los paquetes existentes
sudo yum update -y

# Instalar Docker
sudo yum install -y docker

# Iniciar el servicio Docker
sudo service docker start

# Habilitar Docker para que se inicie en el arranque del sistema
sudo chkconfig docker on

# Agregar el usuario actual al grupo docker para evitar usar sudo
sudo usermod -aG docker $USER

# Reiniciar el servicio Docker para aplicar los cambios de grupo
sudo service docker restart

# Descargar la última versión de Docker Compose desde el repositorio oficial de GitHub
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# Dar permisos de ejecución a Docker Compose
sudo chmod +x /usr/local/bin/docker-compose

# Crear un enlace simbólico a Docker Compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# Verificar si Docker se ha instalado correctamente
docker --version

# Verificar si Docker Compose se ha instalado correctamente
docker-compose --version

echo "Docker y Docker Compose se han instalado correctamente."

# Verificar si el usuario actual se ha agregado correctamente al grupo docker
echo "Reinicia la sesión o cierra y vuelve a abrir la terminal para que los cambios surtan efecto."
